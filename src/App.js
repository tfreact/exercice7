import logo from './logo.svg';
import './App.css';
import Compteur from './Components/Compteur/Compteur';

function App() {
  return (
    <div className="App">
      <Compteur/>
    </div>
  );
}

export default App;
