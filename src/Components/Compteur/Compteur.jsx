import React, { Component, Fragment } from 'react'
import { Button, Alert, Badge } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'; 
class Compteur extends Component
{
    constructor(props)
    {
        super(props);
        this.state ={ count:0};
        // this.onIncrease = this.onIncrease.bind(this);
        // this.onIncrease10 = this.onIncrease10.bind(this);
        // this.onIncrease100 = this.onIncrease100.bind(this);
        // this.onIncrease1000 = this.onIncrease1000.bind(this);
        // this.onDouble=this.onDouble.bind(this);
        // this.onReset=this.onReset.bind(this);
    }

    // onIncrease()
    // {
    //    //  this.state.count=this.state.count+1;

    //     this.setState((state)=>
    //     ({
    //         count: state.count+1
    //     }));
    // }

    // onIncrease10()
    // {
    //     this.setState((state)=>
    //     ({
    //         count: state.count+10
    //     }));
    // }

    // onIncrease100()
    // {
    //     this.setState((state)=>(
    //     {
    //         count: state.count+100
    //     }));
    // }

    // onIncrease1000()
    // {
    //     this.setState((state)=>(
    //     {
    //         count: state.count+1000
    //     }));
    // }
     
    // onDouble()
    // {
    //     this.setState((state)=>
    //    ( {
    //     count: state.count*2
    //     }));
    // }

    // onReset()
    // {
    //     //this.state.count=0;
    //     this.setState(()=>
    //     ({
    //         count: 0
    //     }));
    // }

    calcul(event, valeur)
    {
        const operation = event.target.dataset.operation;
        if(operation==='addition')
        {
            this.setState((state)=>
            ({
                count: state.count+valeur
            }));
        }

        if(operation==='reset')
        {
            this.setState((state)=>
            ({
                count: valeur
            }));
        }

        if(operation==='multiplication')
        {
            this.setState((state)=>
            ({
                count: state.count*valeur
            }));
        }


        
    }
    render()
    {
       const {count} = this.state;

        return(
            <Fragment>
                <style type="text/css">
                    {`
                     
                    .btn-M{
                        padding: 1rem 1.5rem;
                        font-size: 1.5rem;
                        }
                        .btn-l{
                            padding: 2rem 2.5rem;
                            font-size: 2.5rem;
                            }
                    .btn-xl{
                        padding: 3rem 3.5rem;
                        font-size: 3.5rem;
                        }
                    .btn-xxl {
                    padding: 4rem 4.5rem;
                    font-size: 4.5rem;
                    }
                    
                    .btn-xxxl {
                        padding: 5rem 5.5rem;
                        font-size: 5.5rem;
                        }
                        .badge-info {
                            padding: 2rem 2.5rem;
                            font-size: 2.5rem;
                            }
                    `}
  </style> 
                <h1>Compteur</h1>
                <Alert variant="info">
                        <Alert.Heading>Résultat</Alert.Heading>
                        <Badge  variant="info">
                            {count}
                        </Badge><br/> 
                </Alert>   
                <Button data-operation="reset" variant="outline-dark" onClick={(e)=>{this.calcul(e,0)}}>Reset</Button>
                <Button data-operation="addition" size="M"variant="outline-primary" onClick={(e)=>{this.calcul(e,1)}} >+1</Button>
                <Button data-operation="addition" size="l"variant="outline-secondary" onClick={(e)=>{this.calcul(e,10)}}>+10</Button>
                <Button data-operation="addition" size="xl" variant="outline-success" onClick={(e)=>{this.calcul(e,100)}}>+100</Button>
                <Button data-operation="addition" size="xxl" variant="outline-info" onClick={(e)=>{this.calcul(e,1000)}}>+1000</Button>
                <Button data-operation="multiplication" size="xxxl" variant="outline-danger" onClick={(e)=>{this.calcul(e,2)}}>x2</Button>
            
                </Fragment>
            );
    }

}

export default Compteur;